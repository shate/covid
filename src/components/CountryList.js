import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom';
import { Table } from "react-bootstrap";
import { Spinner } from "react-bootstrap";



class CountryList extends React.Component {
    state = {
        loading: true,
        countries: [],
        summary: {}
    };

    async componentDidMount() {
        const summaryUrl = "https://api.covid19api.com/summary";
        const summaryResponse = await fetch(summaryUrl);
        const summaryData = await summaryResponse.json();


        this.setState({
            loading: false,
            summary: summaryData,
            countries: summaryData.Countries,
        });

    }

    render() {
        if(this.state.loading){
            return <Spinner animation="border" role="status">
                <span className="sr-only">Loading...</span>
            </Spinner>
        }

        const countriesJsx = [];
        this.state.countries.forEach(country => {
            countriesJsx.push(
                <tr>
                    <td>
                        <Link to={{
                            pathname: `/country/${country.Slug}/`,
                            state: {
                                summary: this.state.summary
                            }
                        }} >{country.Country}</Link>
                    </td>
                    <td>{country.Slug}</td>
                    <td>{country.CountryCode}</td>
                    <td>{country.TotalConfirmed}</td>
                    <td>{country.TotalDeaths}</td>
                    <td>{country.TotalRecovered}</td>
                </tr>
            );
        })

        const useSortableData = (items, config = null) => {
            const [sortConfig, setSortConfig] = React.useState(config);

            const sortedItems = React.useMemo(() => {
                let sortableItems = [...items];
                if (sortConfig !== null) {
                    sortableItems.sort((a, b) => {
                        if (a[sortConfig.key] < b[sortConfig.key]) {
                            return sortConfig.direction === 'ascending' ? -1 : 1;
                        }
                        if (a[sortConfig.key] > b[sortConfig.key]) {
                            return sortConfig.direction === 'ascending' ? 1 : -1;
                        }
                        return 0;
                    });
                }
                return sortableItems;
            }, [items, sortConfig]);

            const requestSort = (key) => {
                let direction = 'ascending';
                if (
                    sortConfig &&
                    sortConfig.key === key &&
                    sortConfig.direction === 'ascending'
                ) {
                    direction = 'descending';
                }
                setSortConfig({ key, direction });
            };

            return { items: sortedItems, requestSort, sortConfig };
        };


        const ProductTable = (props) => {
            const { items, requestSort, sortConfig } = useSortableData(props.products);
            const getClassNamesFor = (name) => {
                if (!sortConfig) {
                    return;
                }
                return sortConfig.key === name ? sortConfig.direction : undefined;
            };
            return (
                <Table>
                    <caption>Products</caption>
                    <thead>
                    <tr>
                        <th><button type="button" onClick={() => requestSort('Country')} className={getClassNamesFor('Country')}>Country</button></th>
                        <th><button type="button" onClick={() => requestSort('Slug')} className={getClassNamesFor('Slug')}>Slug</button></th>
                        <th><button type="button" onClick={() => requestSort('CountryCode')} className={getClassNamesFor('CountryCode')}>Code</button></th>
                        <th><button type="button" onClick={() => requestSort('TotalConfirmed')} className={getClassNamesFor('TotalConfirmed')}>Total confirmed</button></th>
                        <th><button type="button" onClick={() => requestSort('TotalDeaths')} className={getClassNamesFor('TotalDeaths')}>Total deaths</button></th>
                        <th><button type="button" onClick={() => requestSort('TotalRecovered')} className={getClassNamesFor('TotalRecovered')}>Total recovered</button></th>
                    </tr>
                    </thead>
                    <tbody>
                    {items.map((country) => (
                        <tr key={country.Slug}>
                            <td width={200}>
                                <Link to={{
                                    pathname: `/country/${country.Slug}/`,
                                    state: {
                                        summary: this.state.summary
                                    }
                                }} >{country.Country}</Link>
                            </td>
                            <td width={200}>{country.Slug}</td>
                            <td width={150}>{country.CountryCode}</td>
                            <td width={300}>{country.TotalConfirmed}</td>
                            <td width={300}>{country.TotalDeaths}</td>
                            <td width={300}>{country.TotalRecovered}</td>
                        </tr>
                    ))}
                    </tbody>
                </Table>
            );
        };

        return (
            <div>
                <div>
                    <h1>Global:</h1>
                    <p>Confirmed: {this.state.summary.Global.TotalConfirmed}</p>
                    <p>Deaths: {this.state.summary.Global.TotalDeaths}</p>
                    <p>Recovered: {this.state.summary.Global.TotalRecovered}</p>
                </div>
                <ProductTable products={this.state.countries}/>
            </div>
        );
    }
}

export default CountryList