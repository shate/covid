import CanvasJSReact from "../lib/canvasjs.react";
import { Spinner, Form } from "react-bootstrap";
import React, { useState } from 'react';
import RangeSlider from 'react-bootstrap-range-slider';
import 'bootstrap/dist/css/bootstrap.css';
import 'react-bootstrap-range-slider/dist/react-bootstrap-range-slider.css';



class All extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            rest: [],
            amount: 5,
            loading: true,
            summary: [],
            global: {},
            top: [],
            data: [],
            pieData: []
        }
    }

    handleChange = event => {
        let newAmount = event.target.value;


        this.setState(prevState => (
            {
                amount: newAmount,
            }));
    };

    async componentDidMount() {

        const urlTotal = "https://api.covid19api.com/summary";
        const responseTotal = await fetch(urlTotal);
        const summaryData = await responseTotal.json();

        this.setState({
            summary: summaryData.Countries,
            global: summaryData.Global
        });

        this.state.summary.forEach(country => this.state.top.push({slug: country.Slug, confirmed: country.TotalConfirmed}))
        this.state.top.sort((a, b) => b.confirmed - a.confirmed)

        async function asyncForEach(array, callback) {
            for (let index = 0; index < array.length; index++) {
                await callback(array[index], index, array);
            }
        }



        const start = async () => {
            let total = 0;
            await asyncForEach(this.state.top, async (country) => {
                const urlCountry = "https://api.covid19api.com/total/country/" + country.slug;
                const responseCountry = await fetch(urlCountry);
                const dataCountry = await responseCountry.json();

                let confirmedTotal = []

                let confirmedDaily = []

                dataCountry.forEach(day => {
                    confirmedTotal.push({x: new Date(day.Date), y: day.Confirmed})

                    if(confirmedTotal.length > 1) {
                        let tempConf = (confirmedTotal[confirmedTotal.length - 1].y - confirmedTotal[confirmedTotal.length - 2].y)
                        confirmedDaily.push({x: new Date(day.Date), y: tempConf});
                    }else{
                        confirmedDaily.push({x: new Date(day.Date), y: day.Confirmed});
                    }
                })

                this.state.pieData.push(
                    {
                        y: (country.confirmed / this.state.global.TotalConfirmed * 100).toFixed(2),
                        label: country.slug
                    }
                )
                let restArray = this.state.rest;
                total += country.confirmed;
                restArray.push(total)

                this.state.data.push(
                    {
                        type: "spline",
                        name: country.slug,
                        lineThickness: 3,
                        showInLegend: true,
                        dataPoints: confirmedTotal
                    },)
                this.setState({
                    rest: restArray
                });
                this.setState({
                    loading: false,
                });
            })

        }
        await start();


    }

    render() {

        const pieAll = {
            exportEnabled: true,
            animationEnabled: true,
            title: {
                text: "Recovered cases compared to global"
            },
            data: [{
                type: "pie",
                startAngle: 75,
                toolTipContent: "<b>{label}</b>: {y}%",
                showInLegend: true,
                legendText: "{label}",
                indexLabelFontSize: 16,
                indexLabel: "{label} - {y}%",
                dataPoints: this.state.pieData.slice(0, this.state.amount).concat(
                    {
                        y: ((this.state.global.TotalConfirmed - this.state.rest[this.state.amount - 1]) / this.state.global.TotalConfirmed * 100).toFixed(2),
                        label: "Rest"
                    }
                )
            }]
        }

        if(this.state.loading){
            return <Spinner animation="border" role="status">
                <span className="sr-only">Loading...</span>
            </Spinner>
        }

        let CanvasJS = CanvasJSReact.CanvasJS;
        let CanvasJSChart = CanvasJSReact.CanvasJSChart;
        const total = {
            title: {
                text: "Total Confirmed"
            },
            zoomEnabled: true,
            legend: {
                cursor: "pointer",
                verticalAlign: "top",
                horizontalAlign: "center",
                dockInsidePlotArea: true,
                itemclick: function (e) {
                    //console.log("legend click: " + e.dataPointIndex);
                    //console.log(e);
                    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                        e.dataSeries.visible = false;
                    } else {
                        e.dataSeries.visible = true;
                    }

                    e.chart.render();
                }
            },
            axisX:{
                gridColor: "lightgrey" ,
                gridThickness: 1,
                labelFormatter: function (e) {
                    return CanvasJS.formatDate( e.value, "DD MMM");
                },
                labelAngle: -20
            },
            axisY:{
                gridColor: "lightgrey" ,
                gridThickness: 1
            },
            data: this.state.data.slice(0, this.state.amount)
        }

        const numbers = []
        for(let i = 1 ; i <= this.state.summary.length ; i++){
            numbers.push(<option>{i}</option>)
        }
        return(
            <div>
                <Form>
                    <Form.Group controlId="exampleForm.SelectCustom">
                        <Form.Label>Chose amount of countries:</Form.Label>
                        <Form.Control value={5} as="select" custom onChange={this.handleChange}>
                            {numbers}
                        </Form.Control>
                    </Form.Group>
                </Form>
                <div className="canv">
                    <CanvasJSChart options = {total} /* onRef = {ref => this.chart = ref} *//>
                </div>
                <div className="canv">
                    <CanvasJSChart options = {pieAll} /* onRef = {ref => this.chart = ref} *//>
                </div>

            </div>
        );
    }
}

export default All;