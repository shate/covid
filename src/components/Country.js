import React from "react";
import {Link} from "react-router-dom";
import CanvasJSReact from '../lib/canvasjs.react';

class Country extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            summary: {},
            daily: {
                confirmed: [],
                deaths: [],
                recovered: [],
                active: []
            },
            total: {
                confirmed: [],
                deaths: [],
                recovered: [],
                active: []
            }
        };
    }



    async componentDidMount() {
        const country = this.props.match.params.id;

        const urlTotal = "https://api.covid19api.com/total/country/" + country;
        const responseTotal = await fetch(urlTotal);
        const dataTotal = await responseTotal.json();

        this.setState({
            summary: this.props.location.state.summary,
            loading: false
        });


        let confirmedTotal = []
        let deathsTotal = []
        let recoveredTotal = []
        let activeTotal = []

        let confirmedDaily = []
        let deathsDaily = []
        let recoveredDaily = []
        let activeDaily = []

        dataTotal.forEach(day => {
            confirmedTotal.push({x: new Date(day.Date), y: day.Confirmed})
            deathsTotal.push({x: new Date(day.Date), y: day.Deaths})
            recoveredTotal.push({x: new Date(day.Date), y: day.Recovered})
            activeTotal.push({x: new Date(day.Date), y: day.Active})

            if(confirmedTotal.length > 1) {
                let tempConf = (confirmedTotal[confirmedTotal.length - 1].y - confirmedTotal[confirmedTotal.length - 2].y)
                confirmedDaily.push({x: new Date(day.Date), y: tempConf});
                let tempDeaths = (deathsTotal[deathsTotal.length - 1].y - deathsTotal[deathsTotal.length - 2].y)
                deathsDaily.push({x: new Date(day.Date), y: tempDeaths});
                let tempRec = (recoveredTotal[recoveredTotal.length - 1].y - recoveredTotal[recoveredTotal.length - 2].y)
                recoveredDaily.push({x: new Date(day.Date), y: tempRec});
                let tempAct = (activeTotal[activeTotal.length - 1].y - activeTotal[activeTotal.length - 2].y)
                activeDaily.push({x: new Date(day.Date), y: tempAct});
            }else{
                confirmedDaily.push({x: new Date(day.Date), y: day.Confirmed});
                deathsDaily.push({x: new Date(day.Date), y: day.Deaths});
                recoveredDaily.push({x: new Date(day.Date), y: day.Recovered});
                activeDaily.push({x: new Date(day.Date), y: day.Active});

            }
        })

        this.setState({
            loading: false,
            total: {
                confirmed: confirmedTotal,
                deaths: deathsTotal,
                recovered: recoveredTotal,
                active: activeTotal
            },
            daily: {
                confirmed: confirmedDaily,
                deaths: deathsDaily,
                recovered: recoveredDaily,
                active: activeDaily
            }
        })

    }

    render() {
        if(this.state.loading){
            return (
                <div>Loading...</div>
            )
        }
        let globalConfirmed;
        let globalDeaths;
        let globalRecovered;
        let globalActive;
        let globalConf = this.state.total.confirmed[this.state.total.confirmed.length - 1];
        let globalDead = this.state.total.deaths[this.state.total.deaths.length - 1];
        let globalRec = this.state.total.recovered[this.state.total.recovered.length - 1];
        let globalAct = this.state.total.active[this.state.total.active.length - 1];
        if(globalConf !== undefined && globalDead !== undefined && globalRec !== undefined && globalAct !== undefined) {
            globalConfirmed = {
                exportEnabled: true,
                animationEnabled: true,
                title: {
                    text: "Confirmed cases compared to global"
                },
                data: [{
                    type: "pie",
                    startAngle: 75,
                    toolTipContent: "<b>{label}</b>: {y}%",
                    showInLegend: "true",
                    legendText: "{label}",
                    indexLabelFontSize: 16,
                    indexLabel: "{label} - {y}%",
                    dataPoints: [
                        {y: ((this.state.summary.Global.TotalConfirmed - globalConf.y) / this.state.summary.Global.TotalConfirmed * 100).toFixed(2), label: "Rest"},
                        {y: (globalConf.y / this.state.summary.Global.TotalConfirmed * 100).toFixed(2), label: this.props.match.params.id}
                    ]
                }]
            }
            globalDeaths = {
                exportEnabled: true,
                animationEnabled: true,
                title: {
                    text: "Deaths cases compared to global"
                },
                data: [{
                    type: "pie",
                    startAngle: 75,
                    toolTipContent: "<b>{label}</b>: {y}%",
                    showInLegend: "true",
                    legendText: "{label}",
                    indexLabelFontSize: 16,
                    indexLabel: "{label} - {y}%",
                    dataPoints: [
                        {y: ((this.state.summary.Global.TotalDeaths - globalDead.y) / this.state.summary.Global.TotalDeaths * 100).toFixed(2), label: "Rest"},
                        {y: (globalDead.y / this.state.summary.Global.TotalDeaths * 100).toFixed(2), label: this.props.match.params.id}
                    ]
                }]
            }
            globalRecovered = {
                exportEnabled: true,
                animationEnabled: true,
                title: {
                    text: "Recovered cases compared to global"
                },
                data: [{
                    type: "pie",
                    startAngle: 75,
                    toolTipContent: "<b>{label}</b>: {y}%",
                    showInLegend: "true",
                    legendText: "{label}",
                    indexLabelFontSize: 16,
                    indexLabel: "{label} - {y}%",
                    dataPoints: [
                        {y: ((this.state.summary.Global.TotalRecovered - globalRec.y) / this.state.summary.Global.TotalRecovered * 100).toFixed(2), label: "Rest"},
                        {y: (globalRec.y / this.state.summary.Global.TotalRecovered * 100).toFixed(2), label: this.props.match.params.id}
                    ]
                }]
            }
        }
        let CanvasJS = CanvasJSReact.CanvasJS;
        let CanvasJSChart = CanvasJSReact.CanvasJSChart;
        const total = {
            title: {
                text: "Total"
            },zoomEnabled: true,
            legend: {
                cursor: "pointer",
                verticalAlign: "top",
                horizontalAlign: "center",
                dockInsidePlotArea: true,
                itemclick: function (e) {
                    //console.log("legend click: " + e.dataPointIndex);
                    //console.log(e);
                    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                        e.dataSeries.visible = false;
                    } else {
                        e.dataSeries.visible = true;
                    }

                    e.chart.render();
                }
            },
            axisX:{
                gridColor: "lightgrey" ,
                gridThickness: 1,
                labelFormatter: function (e) {
                    return CanvasJS.formatDate( e.value, "DD MMM");
                },
                labelAngle: -20
            },
            axisY:{
                gridColor: "lightgrey" ,
                gridThickness: 1
            },
            data: [
                {
                    type: "spline",
                    name: "Confirmed",
                    lineThickness: 3,
                    color: "blue",
                    showInLegend: true,
                    dataPoints: this.state.total.confirmed
                },
                {
                    type: "spline",
                    name: "Deaths",
                    lineThickness: 3,
                    color: "red",
                    showInLegend: true,
                    dataPoints: this.state.total.deaths
                },{
                    type: "spline",
                    name: "Recovered",
                    lineThickness: 3,
                    color: "green",
                    showInLegend: true,
                    dataPoints: this.state.total.recovered
                },{
                    type: "spline",
                    name: "Active",
                    lineThickness: 3,
                    color: "orange",
                    showInLegend: true,
                    dataPoints: this.state.total.active
                }
            ]
        }
        const daily = {
            title: {
                text: "Daily"
            },zoomEnabled: true,
            legend: {
                cursor: "pointer",
                verticalAlign: "top",
                horizontalAlign: "center",
                dockInsidePlotArea: true,
                itemclick: function (e) {
                    //console.log("legend click: " + e.dataPointIndex);
                    //console.log(e);
                    if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
                        e.dataSeries.visible = false;
                    } else {
                        e.dataSeries.visible = true;
                    }

                    e.chart.render();
                }
            },
            axisX:{
                gridColor: "lightgrey" ,
                gridThickness: 1,
                labelFormatter: function (e) {
                    return CanvasJS.formatDate( e.value, "DD MMM");
                },
                labelAngle: -20
            },
            axisY:{
                gridColor: "lightgrey" ,
                gridThickness: 1
            },
            data: [
                {
                    type: "spline",
                    name: "Confirmed",
                    lineThickness: 3,
                    color: "blue",
                    showInLegend: true,
                    dataPoints: this.state.daily.confirmed
                },
                {
                    type: "spline",
                    name: "Deaths",
                    lineThickness: 3,
                    color: "red",
                    showInLegend: true,
                    dataPoints: this.state.daily.deaths
                },{
                    type: "spline",
                    name: "Recovered",
                    lineThickness: 3,
                    color: "green",
                    showInLegend: true,
                    dataPoints: this.state.daily.recovered
                },{
                    type: "spline",
                    name: "Active",
                    lineThickness: 3,
                    color: "orange",
                    showInLegend: true,
                    dataPoints: this.state.daily.active
                }
            ]
        }

        return(
            <div>
                <div>
                    <h1>Global:</h1>
                    <p>Confirmed: {this.state.summary.Global.TotalConfirmed}</p>
                    <p>Deaths: {this.state.summary.Global.TotalDeaths}</p>
                    <p>Recovered: {this.state.summary.Global.TotalRecovered}</p>
                </div>
                <h1>Total:</h1>
                <div>
                    <div className="canv">
                        <CanvasJSChart options = {total} /* onRef = {ref => this.chart = ref} *//>
                    </div>
                    <div className="canv">
                        <CanvasJSChart options = {daily} /* onRef = {ref => this.chart = ref} *//>
                    </div>
                    <div className="canv">
                        <CanvasJSChart options = {globalConfirmed} /* onRef = {ref => this.chart = ref} *//>
                    </div>
                    <div className="canv">
                        <CanvasJSChart options = {globalDeaths} /* onRef = {ref => this.chart = ref} *//>
                    </div>
                    <div className="canv">
                        <CanvasJSChart options = {globalRecovered} /* onRef = {ref => this.chart = ref} *//>
                    </div>
                </div>
            </div>
        );
    }



}

export default Country