import React, {Component, useState} from 'react';
import '../App.css';
import CountryList from "./CountryList";
import Home from "./Home";
import {BrowserRouter as Router, Route, Switch, Link} from 'react-router-dom';
import Country from "./Country";
import All from "./All";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Navbar, Nav } from "react-bootstrap";
import { withTranslation, Trans } from "react-i18next";
import { useTranslation } from 'react-i18next';


class App extends Component{
    constructor(props) {
        super(props);
        this.state={
            value: "en"
        };
    }
    handleChange = event => {
        console.log("selected val is ", event.target.value);
        let newlang = event.target.value;
        this.setState(prevState => (
            {
                value: newlang
            }));
        console.log("state value is", newlang);
        this.props.i18n.changeLanguage(newlang);
    };


  render() {
      return (
        <Router>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Navbar.Brand><Link to="/">COVID19</Link></Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                        <Nav.Link><Link to="/countries">Countries</Link></Nav.Link>
                        <Nav.Link><Link to="/all">All</Link></Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
            <div className="container">
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/countries" component={CountryList} />
                    <Route path="/country/:id" component={Country} />
                    <Route path="/all" component={All} />
                </Switch>
            </div>
        </Router>
    );
  }
}
export default App;
